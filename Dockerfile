FROM ruby:latest
ENV LANG C.UTF-8
WORKDIR /app
COPY . /app
RUN cat sources.list > /etc/apt/sources.list;\
    apt-get update;\
    apt-get install -y redis-server;\
    sed -i 's/^\(dir .*\)$/# \1\ndir \/data/' /etc/redis/redis.conf;\    
    bundle install
VOLUME [ "/data" ]
EXPOSE 5050
ENV ENABLE_AUTO_UPDATE yes
CMD ["ruby","main.rb"]