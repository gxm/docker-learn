# encoding: utf-8

$redis = Redis.new

trap("TERM") {
  Thread.start { $redis.bgsave() }
}

class HelloWorldAPI < Midori::API
  get "/" do
    @count = $redis.incr("key")
    Tilt.new("views/layout.haml").render {
      Tilt.new("views/index.haml").render(self)
    }
  end
end

Midori::Configure.bind = "0.0.0.0"
Midori::Configure.port = 5050
Midori::Runner.new(HelloWorldAPI).start
