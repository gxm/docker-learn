# encoding: utf-8
if ENV["ENABLE_AUTO_UPDATE"] == "yes"
  system("git pull && bundle install")
end
system("redis-server /etc/redis/redis.conf")

Encoding.default_external = "UTF-8"

require "openssl"
require "midori"
require "redis"
require "tilt"
require "haml"
require "./model/app"
